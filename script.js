const submitButton = document.getElementById("regiterButton");
submitButton.addEventListener("click", submit);

const confirmPassword = document.getElementById("confirmPassword");
const password = document.getElementById("password");
let matchingPasswords = false;
confirmPassword.addEventListener("input", passwordChange);
password.addEventListener("input", passwordChange);



function submit(event) {
    event.preventDefault();

    const input = validateForm()
    if (input && matchingPasswords) {
        alert("POST request with body: " + JSON.stringify(input));
    }
}

function passwordChange(event) {
    event.preventDefault();

    if (password.value !== confirmPassword.value) {
        matchingPasswords = false;
        confirmPassword.style.borderBlockColor = "red";
    } else {
        matchingPasswords = true;
        confirmPassword.style.borderColor = "black";
    }
}

function validateForm() {
    const input = {}
    let inputFields = document.querySelectorAll(".inputField");
    for (let i = 0; i < inputFields.length; i++) {
        if (inputFields[i].value.trim() === "") {
            inputFields[i].style.borderColor = "red";
            return false;
        }
        inputFields[i].style.borderColor = "black";
        input[inputFields[i].id] = inputFields[i].value;
    }

    let checkbox = document.getElementById("terms");
    if (!checkbox.checked) {
        return false;
    }
    return input;
}